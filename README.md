# leftist-database

Welcome to the Leftist Database, a collection of information for use by leftists
in all of their adventures.

What will be on here?
- Researched articles and statistics on various topics
- Arguments and explanations for various positions and concepts

Why markdown and git verses something else?

First, markdown is a simple, plaintext format, nothing fancy is required to 
produce it, and it is possible to transform markdown pages into other things
like html for websites. For example, sites like [gitlab](https://gitlab.com) and
[github](https://github.com) will render markdown files, making the links
clickable and the formatting nicer. This leads into why use git as the means of
syncing and sharing the project. Git is a decentralized version control system.
Every set of changes that's made to any file in the project is tracked so it's
easy to roll back. Moreover, the project is synced by sending and recieving the
changes themselves, rather than simply overriding the files. This means that
everyone working on the project would have the opportunity to complete "own" the
project. Although it would be convenient to have a centralized version of the
project on a site like github or gitlab, there's no reason why there should be
any "offical" version.

## Making Requests

There are several places to make requests for research or changes to the project.

Issue Types:
* request - New research, article, data, etc.
* revision - Updates, Changes, etc. to existing matieral
* repo - Changes to the repo, either organizational or policy wise

### Gitlab

Make an issue for this repo with the issue type in the subject.

```
request: Data on percent of homelessness since 2011

Description...
```

## How to Contribute

### 1. Fork Repository
- [Gitlab's project forking tutorial](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)

The first step is to create a version of the project that you control, commonly known as a *fork*. You will have total control over what goes into this version of the project and can change, add, or remove what you see fit. You can either make these changes online through gitlab, or you can clone the project to your own device and edit it there.

### 2. Make Changes
- [GitLab Online Editor Tutorial](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)

Front Matter:

This section of the article should contain meta information like the title, tags, other translations of the article, etc.

```
+++
title = "Article"
tags = ["tag1", "tag2", "tag3"]

[translations]
es = "article.md"
fr = "article.md"
+++
```

Images:

Any images needed for your article should be placed in the top level img/ folder

Commit Messages:

After you've finished making your changes, you'll need to write a short description of what you've done.

```
edit: Adds statistics from article to Homelessness page 

* Summarizes changes in rate of homelessness since 2011
* Includes charts displaying levels of homelessness around country
```

The first line, or subject, should be the type of change followed by a present-tense description of what the change is. The body of the commit description should start after one blank line from the subject.

Types of commits:
* edit - Changes content of an existing article
* new - Adds a new article to the project
* remove - Removes an article from the project
* refile - Change in the location of an article within the project

### 3. Submit merge request
- [Gitlab's merge request tutorial](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)

## Resources for Contributing
- [Markdown Tutorial](https://www.markdowntutorial.com)
- [Git Tutorial](https://www.edureka.co/blog/git-tutorial/)
- [GitLab Online Editor Tutorial](https://docs.gitlab.com/ee/user/project/repository/web_editor.html)
