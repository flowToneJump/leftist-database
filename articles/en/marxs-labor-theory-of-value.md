+++
title = "Marx's Labor Theory of Value"
tags = ["LTV", "Economics", "Marxism"]
+++

**Videos on Topic:**
- [Brendan Mcooney's Law of Value Series](https://www.youtube.com/playlist?list=PL3F695D99C91FC6F7)
- [Richard Wolff gives overview of LTV](https://www.youtube.com/watch?v=kyepUAtHAL4)
- [Xexizy's Explanation](https://www.youtube.com/watch?v=67HfnfLYr7U)
