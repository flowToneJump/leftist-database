+++
title = "Summary of Bullshit Jobs by David Graeber"
tags = ["Capitalism", "Jobs"]
+++

# Introduction 

In August 2013, anthropologist David Graeber wrote an article for the radical magazine Strike! It discussed a phenomenon he'd noticed: many people he met weren't just unhappy in their job, but secretly believed the job itself was not contributing anything meaningful to society. The article went viral, and thousands of people commented on articles about the topic.

In 2015, after another round of media hype about the article, British polling service YouGov took a nationwide poll which found 37% of Brits consider their own job to be useless. A later poll in the Netherlands reached 40% useless jobs. In this book Graeber looks further into how bullshit jobs have developed and why there are so many of them in an economic system that is supposed to be efficient.

# Capter 1: What is a Bullshit Job?

**How to define Bullshit Jobs:**

By analysing some jobs that definitely are or probably are not bullshit, Graeber develops a working definition of a Bullshit Job: "[A] bullshit job is a form of paid employment that is so completely pointless, unneccesary or pernicious that even the employee cannot justify its existence even though, as part of the conditions of employment, the employee feels obliged to pretend that this is not the case." (p. 9-10)

**How to know if a job is bullshit:**

Since there is not currently a system to measure anything other than the market value of a job, Graeber argues that employees themselves are good measures of how bullshit their jobs are. Even when they don't know exactly what their jobs do in the grand scheme of the company, workers can usually tell if it's worth doing or not.

It seems that the more power a person has to do harm to the world / society, the more people surround that person to tell them that they're actually doing good. This means that some of the most harmful jobs will not be Bullshit, because those who do them are protected from knowing they are harmful. However, people like Wall Street speculators seem to know they are harmful and don't care.

**The difference between Bullshit and Shit jobs:**

Bullshit jobs are often well paid, but do not contribute to society, while shit jobs are meaningful to society but often badly paid. Some shit jobs are unpleasant to do but fulfilling to have done. Other, like cleaning, are not naturally degrading, but are often made so. These two jobs are different forms of worker oppression.

**Are Bullshit jobs only in the public sector?:**

It turns out there are plenty of bullshit jobs in the private sector too. Because of the widespread use of subcontractors, it is often difficult to tell whether a job even belongs to the public or the private sector. Following neoliberal 'market reforms' tends to lead to more bureaucracy. For some reason when corporations cut workers it tends to be the actually productive workers on the bottom rung, not the paper-pushers in the higher ranks.

**Hit Men, Hairdressers, and the service industry:**

A hit man is probably not an official paid employee, and even if they think their job is harmful to society, they probably don't feel the need to pretend it is useful.

The labelling of hairdresser as a bullshit job often is traced back to a Douglas Adams novel, but it is factually incorrect. Hairdressers themselves see their work as valuable, and hair salons themselves serve an important community function, especially in poorer neighbourhoods. It seems there is some measure of sexism and snobbery involved in labelling hairdressers as useless to society.

It seems that most people working in the service industry do not consider their jobs bullshit, with a few exceptions. IT service providers and telemarketers "were convinced that they were basically involved in scams." A number of sex workers wrote to Graeber to describe those jobs as bullshit, since they made more money in a few years as a stripper than they were expected to make in their entire academic career. Graeber argues that this does not make sex work bullshit, but rather shows that we live in a bullshit society that undervalues both women and academic work.

**On the rise of Bullshit in non-Bullshit Jobs:**

"[T]here are very few jobs that do not involve at least a few pointless or idiotic elements" (p. 23) and these elements seem to be increasing. But it seems to have increased more in middle-class employment than traditionally working-class employment. However, in working class employment it is rising fastest in female-dominated fields of care-giving, like nursing.

An American survey of office workers indicates that they spend less than half their working hours doing they actual jobs, with the rest being eaten up by administration, emails, and meetings.

While this book is about jobs that are 100% bullshit, it is clear that there are too few productive working hours available to give everyone a 40-hour workweek. We really should be changing to a 15-20 hour workweek, like the Economist James Maynard Keynes predicted in 1930.

# Chapter 2: What Sort of Bullshit Jobs Are There?

The polls that were mentioned in the introduction provided only a little information about the participants, and sadly did not include their actual job description. So David Graeber collected the most detailed responses to the various articles written about his idea, and asked people to email him if they thought they had a bullshit job. With a few hundred testimonies, Graeber had enough information to form 5 categories of bullshit jobs: flunkies, goons, duct tapers, box tickers, and taskmasters.

**Flunkies:** 

"Flunky jobs are those that exist only or primarily to make someone else look or feel important" (p. 28).

Doormen and elevator make businesses look more important, as do receptionists at companies where there are so few calls it makes no sense to hire an extra person for it. Receptionists are expected, so the business hires one.

Other flunkies are hired specifically to make the person above them look more important. But sometimes a position that starts as a flunky role becomes crucial, because the people above the flunky decide to delegate most or all of their important tasks. This has traditionally happened to female secretaries. In some business structures, a high-flyer's importance will be measured not by the actual work they do, but by the amount of people they can boss around.

**Goons:** 

Graeber describes goons as "people whose jobs have an aggressive element, but, crucially, who exist only because other people employ them (p. 36)"

"Lobbyists, PR Specialists, telemarketers, and corporate lawyers" are among the kinds of jobs that would probably not be necessary if other companies didn't have them first. And the world would probably be better without them. The goons tend to dislike their jobs because they feel they are actively harmful. The fields of advertising and publicity in particular resent their work so much they made a magazine, Adbusters, to criticise their own work. Call centre employees also dislike the manipulation they have to perform in their jobs, selling thing to people who don't want them or providing bad / incomplete information.

**Duct Tapers:**

"Duct Tapers are employees whose jobs exist only because of a glitch or fault in the organisation; who are there to solve a problem that ought not to exist" (p. 40).

There were always be some flaws in systems, so there will always be a necessary amount of duct taping required to keep an organisation running smoothly. Similarly, a builder will sometimes have to interpret an architect's design to make it actually function as a building. But the duct taper truly has a bullshit job when their entire job consists of problems caused by "sloppy or incompetent supervisors," or when their job could easily be automated.

**Box Tickers:** 

Box tickers are described as "employees who exist only or primarily to allow an organisation to be able to claim it is doing something that, in fact, it is not doing" (p. 45)

Box ticking is especially miserable because the employee is often actively undermining the outcome they must pretend to aim for. They have to stick to rigid rules and protocols regardless of costs in time and energy to all the people involved. "This mentality seems to increase, not decrease, when governments functions are reorganised to be more like a business, and citizens, for example, are redefined as 'customers'" (p.48).

Another important aspect of box ticking is making the report look physically attractive, especially in the public sector. Some executives employ people specifically to make their reports and presentations look important. Sometimes this even extends to in-company magazines or tv channels.

**Taskmasters:** 

Taskmasters come in two distinct flavours: The ones who tell their underlings what to do, and the ones who actively invent bullshit work for those underlings to do.

The first flavour is only bullshit if the taskmaster knows that the underlings would be doing the work anyway. They often have to ensure people hit their 'productivity metrics' and send reports up the chain of command. Since this often requires come creative work with numbers, they might actually be able to save the jobs of people who are not being assigned much work.

The second is harmful because it makes other bullshit jobs. It usually involves not just the numerical results that type 1 taskmasters manage, but a whole range of appraisals, reviews, assessments, and other such work that the underlings have to perform. This means that teachers, for instance, have to spend time filling in forms about their work and results, instead of being able to focus on their lesson plans and evaluating the work of their students.

Sometimes a worker in a non-bullshit position has tenure but is incompetent. A taskmaster may have to invent an entirely new role in the corporation just to hire someone to do the actual non-bullshit work, but in secret.

**It's complicated:**

Not all jobs are easily classified as one of these five categories. In fact, many of them overlap and sometimes a job will gradually shift from one category to another.

There are also some ambiguous categories where it is not sure if they qualify as individual categories or even as bullshit. One of those is the Imaginary Friend: "People hired ostensibly to humanise an inhuman corporate environment" (p. 58), who have to design team-building games or wear costumes to pretend the corporation is fun. Another is the Flak Catcher: "subordinates hired to be at the receiving end of often legitimate complaints" (p. 60) specifically because they can't actually solve the problems. This can be a dangerous job, but it might actually be providing an important service to the company and thus not be entirely bullshit.

**Second Order Bullshit Jobs:**

Some jobs and tasks are not of themselves bullshit, but only exist because of bullshit happening higher up the food chain. Telemarketing offices still have to be cleaned even though telemarketing is -mostly bullshit.

Between the real bullshit jobs, the second order bullshit jobs, and the bullshit in non-bullshit jobs, there are a lot of wasted hours. If we cut all of those out, we could probably get down to a 15-hour workweek and still get every worthwhile task done.

**Is it possible to have a bullshit job and not know it?:**

It probably is. To find the exact borders of what makes a bullshit job would require far more data, and thus far more and detailed surveys that is available now. So we do not yet know why some people in a field are conviced their jobs have value when many others in the same field believe they do not. It seems most people in control of hiring and firing are resistant to the idea of bullshit jobs even existing. They prefer to believe that such jobs could never exist under a market economy. They themselves might not have bullshit jobs, but perhaps they are "blind to all the bullshit they create" (p. 65).

# Chapter 3: Why do Those on Bullshit Jobs Often Report Themselves Unhappy?

**On Spiritual Violence, Part 1:**

"One might imagine that those being paid to do nothing would consider themselves fortunate" (p. 67), but not only are they unhappy, they don't seem to know why.

In some cases this can be explained by different expectations of different social classes. Employees from a working class background might see a pointless job with a lot of empty time. Those from a professional background might see the same empty time as potential for networking and finding their next job.

Some people felt as if they were scamming their bosses and, even when these bosses seem to approve, that increases the feelings of uselessness. Students were particularly frustrated by having to pretend to work or do useless tasks when they could have used the downtime for their actual studies. But maybe there is a deeper reason for the resistance people feel to their bullshit jobs.

**Flaws in the classical economic theory:**

The classical economic theory assumes every human is motivated by "a calculus of all costs and benefits" (p. 80), and they make predictions based on this theoretical human, the homo oeconomicus. This theory argues that humans "have to be compelled to work," and that "if humans are offered the choice to be parasites," they will take it (p. 81).

The results of psychological research don't agree, however, and humans prove to be social very social animals. Since the early 1900s, psychologists have been learning about the joy babies feel when they realise they can cause an effect. In fact, children seem to develop their sense of self from cause and effect. But it also seems to be very distressing for a child to learn about an effect they can cause, and then not be allowed to repeat it. This indicates that not being able to do anything meaningful in their job might be very distressing for adults.

**The human work pattern and how it changed:**

Until quite recently in our history, "normal human work patterns [took] the form of periodic intense bursts of energy, followed by relaxation, followed by picking up again toward another intense bout" (p. 86). The focus of work was on the result, and the time between a the start and finish was largely unsupervised. In fact, the very idea of someone being able to own your time was strange. Wage labour was considered degrading in the societies that had it, and in the Middle Ages was mostly performed by 'unfree' labourers.

The entire concept of time was different. "In places without clocks, time is measured by actions rather than action being measured by time" (p. 90). In Europe, the concept of time as fixed and measurable spread by way of clock towers in the 14th century, which were often closely associated with the merchant guilds. But it wasn't until the late 1700s that personal clocks and pocket watches appeared, alongside the industrial revolution. When time became divisible, it also became possible to sell your time, or to buy that of others.

Since it was now also possible to 'waste' time, working according to the old rhythm was increasingly seen as immoral. And in their efforts to improve their working conditions, workers had to adopt the same language as their superiors. "But the very act of demanding 'free time' . . . had the effect of reinforcing the idea that when a worker was 'on the clock,' his time truly did belong to the person who had bought it" (p. 92)

**The resentment:**

Modern morality still holds on to this notion that submitting to authority even in meaningless work somehow makes you a better person. "But ultimately, the need to play a game of make-believe not of one's own making, a game that exists only as a form of power imposed on you, is inherently demoralising" (p. 99).

# Chapter 4: What Is It Like to Have a Bullshit Job?

**On Spiritual Violence, Part 2:**

A highly infuriating aspect of bullshit jobs is their ambiguity: it is often quite difficult to tell who else exactly knows your job is pointless, what they expect you to do with it, or even how much you can talk about it to others.

**On make-believe and ambiguity:**

"[A] lot of jobs require make-believe" (p. 105). The required friendliness of service jobs is the most obvious example, but many bullshit jobs require an amount of pretending to be productive. Unlike in service jobs, however, it is usually not clear quite how much fakery is required.

Sometimes supervisors will be understanding and mildly supportive of people doing their own thing, but that's not the most common experience. Likewise, sometimes co-workers can talk relatively openly about their experiences, but in most of the responses workers felt alone in their misery.

**On the social, mental, and physical result of a lack of purpose:**

"[M]ost people in the world today, certainly in wealthy countries, are now taught to see their work as their principal way of having an effect on the world" (p. 113). This is tied to the idea that you get paid for doing something meaningful. This explains why many of responses described frustration in discovering their job lacked a purpose.

While some people mostly experience boredom, others develop a deep anxiety. Multiple of the responses described a high level of tension in companies where people felt pressured to hide their feelings of purposelessness. In some cases, this lead to harmful power plays and verbal abuse of workers by their bosses.

Many people reported stress-related illness and injury, from depression and anxiety to "carpal tunnel syndrome that mysteriously vanishes when the job ends" (p. 120) and other systemic physical issues.

**On the guilt of feeling miserable:** 

"Even in relatively benign office environments, the lack of a sense of purpose eats away at people" (p. 123). Being in a well-paid or well-respected position does not reduce these feelings, in fact it seems to make them worse. This can be due to the value outsiders place on the job, or due to a complicated kind of gratitude for a job that pays the bills.

There is also a phenomenon called 'rights-scolding,' where people are criticised for expecting to be treated well when others are treated worse. This is very clear in the general media commentary on younger people wanting state welfare by those who grew up with such state welfare.

**On knowing that one is doing harm:**

Some responses were from people who had to pretend their job was useful not just when it was pointless, but when it was actually harmful. This is "most common among social service providers who work for government or nongovernmental organisations (NGOs)" (p. 131).

Such 'services' in reality include denying people disability benefits, getting homeless people kicked out of shelters, and sending out letters with deliberate mistakes to vulnerable people for monetary gain.

**On spiritual resistance:**

While these harmful jobs are obviously the worst on a spiritual level, most bullshit jobs can fairly be described as soul-destroying. But there are many forms of resistance. Some find alternatives to keep themselves busy at work, by starting businesses, having elaborate day-dreams, editing Wikipedia, or taking up creative hobbies.

There is not enough data on the different forms of spiritual resistance, but a few trends seem to emerge. For one, the new skills people pick up are not usually the ones they were supposed to be hired for. Secondly, many people struggled to transform their useless hours at work into hours they could actually use for their own goals. For many people, the mental strain of their meaningless work drives them to social media and consumable culture that fits the secret moments they can reclaim.

Some lucky workers managed to protect their minds by restricting their bullshit jobs to part-time work and using it to fund more fulfilling projects. "Others turn to political activism" which psychologists indicate "can be extremely beneficial to a worker's emotional and physical health" (p. 141).

# Chapter 5: Why Are Bullshit Jobs Proliferating?

Graeber starts this chapter by explaining why the growth of bullshit jobs wasn't noticed. The first part is the widespread belief that the neoliberal market-led system is supposed to be the most efficient system. Therefore many people refuse to believe that bullshit jobs could even exist under this system.

The second is the language used around employment, which has in effect hidden certain aspects of the growth. Economists talk about the massive growth of the service sector, but "the proportion of the workforce made up of actual waiters, barbers, salesclerks, and the like was really quite small" (p. 148). It has also remained quite stable over the past century. The real growth has occurred in what could be labelled the 'information sector': administration, accounting, consultancy, IT, etc. And this is where most bullshit jobs appear.

**On causality and the sociological levels of explanation:**

To get to the bottom of why bullshit jobs exist and are growing in number, we need to look at the different types of causes that work together. 1. "On the individual level, why do people agree to do and put up with their own bullshit jobs? 2. On social and economic levels, what are the larger forces that have led to the proliferation of bullshit jobs? 3. On the cultural and political levels, why is the bullshitization of the economy not seen as a social problem, and why has no one done anything about it?" (p. 154) While a lot of people will try to use only one of these questions to discuss the wider topic, it is important to remember that they all work at the same time. The last one is ignored most often, because it involves looking at what is not happening instead of what is.

This chapter is about the social and economic levels; the final two will discuss the cultural and political levels.

**On direct and indirect social engineering:** 

The Soviet Union set out a central directive to create as many jobs as possible to ensure full employment. While capitalist regimes do not send out such directives, policy throughout most of the past century has encouraged the creation of new jobs. But while 'More Jobs' is a slogan most people can get behind, "they never specify those jobs will be good for anything; it's simply assumed that if the market produced them, they will be" (p. 156).

President Obama argued against a single-payer health care system by stating that it would costs up to 3 million jobs. An openly inefficient system was deemed preferable to finding more useful jobs for all those people.

**On the two types of false explanations:** 

Market enthusiasts propose two types of explanations for pointless employment. The first type states that jobs in private businesses are never bullshit, because the market would not allow for that to happen. They claim the value of the jobs are just hidden from the employee by the complicated structure of modern work divisions.

The second type allows that jobs in the private sector can also be bullshit, but argues this is possible exclusively because governments impose some regulations on the market. This fundamentalist free market view cannot be proven or disproven.

Graeber uses the example of American universities to counter bother these arguments: Not only has the amount of support staff grown far more than the amount of students, professors, or degrees, but it has increased more in private universities than public ones. Political scientist Benjamin Ginsberg argues that university administrators "effectively staged a coup" in the 80s, which also saw the rise of finance capitalism. To see how this happened, we need to go back to the FIRE sector (finance, insurance, real estate).

**Why the financial industry is mostly a scam:**

Companies that are supposed to distribute compensation payments become massive instant bureaucracies. After all, they get paid from the same money that should be going to claimants.

"Of course, this is basically what the entire FIRE sector does: it creates money (by making loans) and then moves it around in often extremely complicated ways, extracting another small cut with every transaction" (p. 167). Common themes in the stories of bank workers were highly bloated middle management, artificial contests to increase the standing of the company, and forced charity actions for the same reason. The workers also rarely knew what their work contributed to the banks.

One respondent was actually hired to analyse banks and improve their efficiency, but non of his solutions were ever put into action. Graeber argues that the structure of banks no longer resemble traditional companies, but seem to be "made up of a series of feudal retinues, each answerable to a lordly executive" (p. 175).

**On managerial feudalism:**

"Under classic capitalist conditions [it] makes no sense to hire unnecessary workers. … But by a feudal logic, where economic and political considerations overlap, the same behaviour makes perfect sense" (p. 176). A large pool of underlings can be considered both a visible measure of a manager's worth and leverage in office politics.

There is one crucial difference: in medieval feudalism the workers were largely unsupervised. Even when working in guilds or universities, the worker would be overseen by someone in the same field. "Efficiency has come to mean vesting more and more power in managers, supervisors, and other presumed 'efficiency experts'" (p. 178)

It was common practice in the 50s, 60s, and 70s that, when a business improved its efficiency, the workers would share in the increased profits. But since the 80s, this profit is not only going to the wealthy 1%, but also to managerial positions and administrative staff that were not, in fact, present when the productivity increased.

**On managerial feudalism in the creative industries:**

The feudal fondness for installing hierarchies also seems well reflected in the current managerial style. 'Creative industries' are being dominated by "managers whose basic job is to sell things to one another" (p. 182). This has been observed in managers at academic presses, curators in the visual arts, and journalism 'producers.' But the trend is clearest in film and television.

"[W]hat was once the fairly straightforward business of pitching and selling a script" has now become a gauntlet of middle management executives who all have to have their opinions and input. This is true not just for the new companies that exclusively write ideas for tv and film, but also for in-house writers. But executives are unwilling to commit to a yes or no, and project limbo can last multiple years. "The longer the process takes, the greater the excuse for the endless multiplication of intermediary positions, and the more money is siphoned off before it has any chance to get to those doing the actual work" (p. 187).

Graeber restates that he thinks the same process is slowing down technological progress, since scientists have to spend a lot of time trying to convince people to fund their research. For more on that, see Utopia of Rules.

**Conclusion:**

"There seems to be an intrinsic connection between the financialization of the economy, the blossoming of information industries, and the proliferation of bullshit jobs" (p. 191). But this structure is outside of the normal corporate capitalist system, which up to the 70s still saw a big divide between corporate executives and the financial industry. Perhaps the addition of this managerial feudalism explains why the system as a whole has become so confusing.

# Chapter 6: Why Do We as a Society Not Object to the Growth of Pointless Employment?

"We could all easily be putting in a twenty- or even fifteen-hour workweek" (p. 194). Not only are we wasting human life-hours on pointless work, reducing work-hours would benefit the environment.

Since economics developed largely out of the field of moral philosophy, and that developed from theology, there are some ideas about work that are simply so ingrained that they are rarely questioned. So this chapter will question the origins of attitudes towards labour. But it will start with an evaluation of 'value'.

**On value:**

The word value does not have a clear definition, which complicates discussions about the value of work. "Economists measure value in terms of what they call 'utility', the degree to which a good or service is useful in satisfying a want or need" (p. 196). But that definition brings it's own complications, since there is not really a definition of what humans need, beyond the basics required to literally keep them alive. "To a large degree, needs are just other people's expectations" (p. 197).

The labour theory of value states that the work that goes into producing a commodity gives it it's value. But this does not mean that everything that requires work necessarily produces value. Economists do tend to believe that if there is a market for something, it must necessarily have some value to somebody. Most working people would consider work valuable when it improves people's lives or answers a demand, but not if it only creates demand by way of charging interest on debts or personal insecurities. But what, then, does it mean to 'improve people's lives?'

**On social value:**

Economists tend to argue that the price of a commodity will approximate its 'real' market value, and also that whatever price it tends to have must be said actual value. Some anticapitalists tend to argue that capitalism is a total system, and that commodities therefore cannot have a 'real' value outside the system. But while it is true that one cannot 'leave' capitalism by their consumption, capitalism does not shape our entire existence. This means that it is still possible to make choices based on what you find important, a.k.a. your values.

'Values,' then, is treated as a different concept than the singular 'value,' which is generally used for economic matters. While you can compare commodities directly by means of their monetary value, you cannot precisely compare things within your values, even within the same one. You cannot precisely calculate how much happier one song makes you than another. These values together can be named social value.

But while the economic sense of value has only existed since the advent of money, economic value and social value are not separate. In fact, they seem to be inversely related: "[t]he more your work helps and benefits others, and the more social value you create, the less you are likely to be paid for it" (p. 207).

**On the inverse relationship between social and economic value:**

If all the people who did certain kinds of jobs, like cleaning, child care, or teaching, were to suddenly disappear, the world would very quickly notice the bad results. But there are plenty of jobs that the world could probably do without for a while, such as lobbyists or marketing gurus. As Rutger Bregman describes in Utopia for Realists, a 6-month banking strike in Ireland did not lead to an economic crash, while less than two weeks of strikes by garbage collectors in New York led to an unliveable city. But the less useful jobs tend to have far higher salaries than the useful jobs.

A few economists have tried to measure the social values of certain professions, and contrasted those social values to their salaries. These studies confirm this principle that the more socially valuable jobs are rewarded less. There are no simple economic explanations for this. Clearly "education levels are very important in determining salary levels" (p. 212), but that does not explain how so many people with PhDs live below the poverty line in the US. Supply and demand doesn't explain it either, since nurses are paid quite poorly despite a shortage.

Several ideas seem to be working together at the root of this issue. For one, there is an assumption that working for money is a strong virtue, and that not working for money is an even stronger vice. Then there is a sense that work is necessarily something we do not like to do. This suggests that everything we actually like to do can not really be considered a job but a hobby, and should not be paid. If you take this to its logical conclusion, anyone who has chosen a job that they actually want to do should be paid less than those who hate their work. And since workers in fields such as healthcare and teaching are generally passionate about their work, they are paid very little.

**On theological attitudes to work:**

"In both the story of the Garden of Eden and in the myth of Prometheus, the fact that humans have to work is seen as their punishment for having defied a divine creator, but at the same time" work gives humans the ability to produce things for themselves, and "is presented as a [reflection] of the divine power of Creation itself" (p. 221). However, a lot of work doesn't produce anything, and even producing jobs don't so much 'create' new object as rearrange bits that already exist. Graeber therefore argues that defining work by 'productivity' is pretty much theological.

The problem with this definition of productivity is that it practically erases all non-productive work, which disproportionately affects women. But it also erases the very real work that goes into making things, and children. It implies that objects magically emerge from factories, and babies magically emerge from women's bodies. But production is not the only economic concept with a theological basis. Saint Augustine argued that humans are naturally in a state of competition due to scarcity, and that belief is mainstream economic thought today.

**On historical attitudes to work:**

The most important to the discussion of the attitudes to wage labour is the concept of 'service.' The European Middle Ages saw the rise of 'life-cycle' service, in which people spent their adolescence and early adulthood in the service of someone else. This service was expected of boys and girls alike, and of all classes. The service was paid, and the worker learned a trade or how to manage a house-hold, which they would be able to both afford and run independently when their service ended. Simultaneously, this was a training for maturity.

With the advent of capitalism, that is "the gradual transformation of relations of service into permanent relations of wage labor, . . . millions of young people found themselves trapped in a permanent social adolescence"(p. 226). When they rebelled and left these positions to start their own households, the employing middle classes experienced a moral panic over the heathenish lives of the lower classes. Since they could no longer offer the workers the promise of a future independent career, they started preaching the moral value of work instead.

After the industrial revolution, this led to the 'gospel of work,' in which work is said to be of utmost value to god. And of course, such honourable work should be its own reward and not require good pay. However, workers themselves realised that "everything that made the rich and powerful people rich and powerful was, in fact, created by the efforts of the poor" (p. 230). In fact the labour theory of value was understood and taken up very widely after the industrial revolution. This, of course, prompted economists to start looking for another explanation of value.

The Labour Theory of Value was particularly powerful in the American colonies of Britain, where the workers saw imperialists take the value they produced and became soldiers in the War of Independence. In fact, anticapitalist sentiments were quite high after independence, and corporations had to prove they were working for a public benefit before they could be approved. Even on the Western Frontier they spoke openly about the Labour Theory of Value and socialist theory.

**On the flaw of the labour theory of value:**

Graeber suggests that the rise of corporate capitalism and the changes in popular consciousness that made this possible are due to a flaw in the Labour Theory of Value. This flaw is the focus on 'production,' which, as mentioned earlier, erases most of women's work but also erases a lot non-factory jobs. It also created a framework in which machines became more important than the people operating them in terms of wealth creation. This meant the owning classes could now be labelled as creators of wealth, rather than the working classes.

Cab drivers, teachers, and cleaners are not really 'productive' jobs, but they certainly are working class jobs. A large section of working class jobs deals explicitly with caring for the wants of others. And even in jobs that we might label productive, such as bricklayer, requires the labourers to be aware and mindful of what their boss wants of them. The boss, on the other hand, is not required by his function to care much about the wants of his workers. "To think of labor as valuable primarily because it is productive . . . allows one to make all this disappear" (p. 237).

For the modern worker's movement, the Labour Theory of Value recognises that society is also something that we make, and that we could hypothetically choose to make otherwise. But it seems that many forms of care require the ability to plan ahead, and thus a world that is at least moderately predictable. Thus, "love for others . . . regularly requires the maintenance of institutional structures one might otherwise despise" (p. 239).

**On work as self-sacrifice:**

To find meaning in jobs where they are treated impersonally and as a replaceable part of a machine, workers are once again being told that waged labour breeds character. It is a form of sacrifice of self to make you worthy enough to be an adult consumer. In this train of thought, middle class workers especially "gain feelings of dignity and self-worth" not despite hating their jobs, but "because they hate their jobs" (p. 242)

This also leads to vilification of everyone who does not adhere to this 'work to have value' standard. "Suffering has become a badge of economic citizenship" (p. 243) and anyone poor or unemployed or in need of assistance is considered unworthy of respect.

# Chapter 7: What Are the Political Effects of Bullshit Jobs and Is There Anything That Can Be Done About This Situation?

Our society seems to have shaped itself around the sense that we must suffer at work to 'deserve' things we enjoy. But this has also lead to culture and entertainment that can be consumed in the leftover time, "'compensatory consumerism' . . . to make up for the fact that you don't have a life" (p. 247).

Politicians profit from the resentment that bullshit jobs breed between employed and unemployed workers, those holding productive or bullshit jobs, underpaid or glamorous jobs. They profit because all this resentment keeps people divided and distracted from the underlying causes.

**On moral resentment:**

One complicated form of resentment is the 'moral envy': "feelings of envy and resentment directed at another person, not because that person is wealthy, or gifted, or lucky, but because [their] behaviour is seen as upholding a higher moral standard than the envier's own" (p. 248). This is particularly prevalent in activist and religious communities, but seems to also pervade the attitudes towards work. Of course, immigrants are portrayed as lazy scroungers and yet also as stealing all the jobs, and poor people are portrayed as lazy scroungers who are working 'good' non-bullshit jobs.

But another aspect of this is the bizarre opposition in the US to strikes by autoworkers and especially teachers. These are clearly jobs of great social importance, and yet their calls for better working conditions are vilified by vast swathes of the US population. It's as if the knowledge that one has a socially valuable job should be enough, and wanting good working conditions and payment on top of that is just too much.

**On the troops and the liberal elites:**

In the US, it seems there is only one working-class position in which people are allowed to feel fulfilled and get a decent pay, and that's the military. According to right-wing populism, the military is absolutely above reproach, and the enemy is the 'liberal elite.' This is not without reason however.

It is socially acceptable to insult the white working classes in ways that would be obvious bigotry if aimed at any other group. And while these people may be able to imagine themselves or their children suddenly becoming wealthy, it is pretty clear that there are certain jobs they will never be able to enter. Many meaningful, high-paying jobs require not only money for an expensive education, but an immense amount of social capital to network their way in. They have seemingly become "a new American nobility" with a "hereditary right" to these jobs (p. 253).

The military is the only remaining field where one can get a decent compensation and feel like there is a humanitarian goal to one's work. And while that seems like a bad reason to enter the industry of war, even the Peace Corps requires a college degree. Supporting the troops and demonising the 'liberal elite' also helps the right wing effectively label the left as hypocrites, because they now support a hierarchical structure where they previously talked of tearing them down.

But perhaps more importantly, the 'support the troops' slogan is pure optics, since in the long run military personnel gets very little out of their service. The same right-wing populists seem "strangely indifferent to the fact that a large percentage of them end up spending the rest of their lives homeless, jobless, impoverished, addicted, or begging with no legs" (p. 257).

**On robotization and bullshit jobs:**

Many stories about mechanisation have looked at the risks and rewards of automation. But not only have they have usually overlooked the caring aspect of labour, they tend to stop short of automating away all the jobs at the top of the hierarchies and removing the need for capitalists.

It is true that many production jobs could be automated, but "caring consists mainly of the sorts of things most of us would least like to see done by a machine" (p. 257), and that "cannot be quantified" (p. 262). In fact, the attempt to make certain jobs quantifiable is exactly what leads to bullshit work, because "it takes enormous amounts of human labour to render the processes, tasks, and outcomes that surround anything of caring value into a form that computers can even recognise" (p. 262).

**On the politics of bullshitization:**

Many of the previously mentioned stories predicted catastrophic results from mass unemployment following automation, but there is no reason for it to cause a crisis. In fact, much of the automation has already happened, but we've invented a lot of pointless jobs to make up for it. But there is no reason why people shouldn't be able to handle more free time, and no economic reason to keep so many people employed on pointless tasks. So the reason for the current status quo must be political.

Before the industrial revolution, it seems the economy was seen as a tool to create the best society. But nowadays in many of the biggest US cities, hospitals and universities are the biggest employers, with all the managerial feudalism that implies, and a large section of the economy. The mainstream Left is dominated by these managerial classes and 'control' the production of humans. The mainstream Right is dominated by anti-intellectual populists who control the production of things.

Because US unions are often in-company systems that represent management more than the actual workers, they don't have much of a voice in the current mainstream Left, and have to either accept the bullshit status quo or join the right-wingers.

**On the option of a Universal Basic Income:**

Graeber is suspicious of policy as a concept, because it "implies the existence of an elite group . . . that gets to decide on something (a policy) that they then arrange to be imposed on everybody else" (p. 270). The one solution with any popular backing that would actually decrease the government imposition is the Universal Basic Income. This UBI calls for "replacing all means-tested social welfare benefits with a flat fee to be paid to everyone, equally" (p. 274).

A UBI would reduce the need for all the jobs that merely serve to keep people from claiming. This seriously adds up when you include not only the programmes that investigate claims or give unemployment training, but also all the NGOs and charities that try to help people navigate the labyrinth of paperwork that results from trying to standardise caring work. And the system keeps thousands of people employed by not giving people the benefits they deserve. After all, currently 60% of UK citizens eligible for unemployment benefits are denied.

"Huge sections of government - and precisely the most intrusive and obnoxious ones, since they are most deeply involved in the moral surveillance of ordinary citizens - would be instantly made unnecessary and could be simply closed down" (p. 280). In effect, the UBI would "detach livelihood from work" (p. 279) which opens up space for pointless jobs to actually disappear, since they no longer serve even the basic purpose of providing the worker with the means to survive.

Of course, care must be taken to not install the conservative 'free-market' version of a UBI, but a realistic version with an eye to externalities such as the possible need for rent control. And perhaps in time it could open up ways to distribute goods without even needing money.

On UBI and this book A system of unconditional support would influence two important issues that this book has discussed. First, the abusive nature of hierarchical structures in bullshit jobs: "All of the gratuitous sadism of workplace politics depends on one's inability to say 'I quit' and feel no economic consequences" (p. 283). Beyond this personal freedom, having an unconditional income gives the workers of bullshit jobs as a collective the chance to speak up and organise, and so abolish these pointless positions entirely. This newfound freedom to think about one's situation could have political ramifications as well.

And finally: "If we let everyone decide for themselves how they were best fit to benefit humanity, with no restrictions at all, how could they possibly end up with a distribution of labor that is more inefficient than the one we already have?" (p. 285).
